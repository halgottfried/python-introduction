__author__ = 'halgottfried'

try:
    name = raw_input('What is your name and the year you were born \n (Format: First Name Last Name YOB) ?')
    firstName, LastName, Year = name.split()

except(ValueError):
    print "That is not enough items."
    print "Format: First Name <space> Last Name <space> Year of Birth"
    # Exits our program if the error is caught.
    raise SystemExit

else:
    def hasnumbers(name):
        return any(char.isdigit() for char in name)

if hasnumbers(firstName) or hasnumbers(LastName):
    print ('I am sorry but you seem to have spelled your name wrong')
    raise SystemExit
else:
    firstLen = len(firstName)
    lastLen = len(LastName)

    print ('The entered first name ' + (firstName) + ' is ' + str(firstLen) + ' letters long.')
    print ('The entered last name ' + (LastName) + ' is ' + str(lastLen) + ' letters long.')

try:
      age = int(2014) - int(Year)
      assert(age >= 0)
      print 'Your age is: ' + str(age)
      #Not very elegant should have used datetime but it works.

except(ValueError, AssertionError):
     print 'Your age is not a correct value'
     raise SystemExit