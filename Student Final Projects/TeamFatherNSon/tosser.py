import random

heads = True
tails = False

def flipper():
    """ this function flips the coin """
    pass

def compare(tossed, guessed):
    """ compares the 'tossed' coin to the user's guess """
    if tossed == guessed:
        print("You guessed right!")
        return True
    else:
        print("sorry, you guessed wrong")
        return False


def guess():
    """ retrieves user's guess """
    
    try:
        guessed = raw_input("was it (h)eads or (t)ails?")
        guessed = guessed.lower()
        if guessed == "heads" or guessed == "h":
            return True
        elif guessed == "tails" or guessed == "t":
            return False
        else:
            print("you must guess 'heads' or 'tails'. try again")
            guess()
    except:
        print("Error occurred, you must guess 'heads' or 'tails'. try again")
        guess()

def toss():
    """Function creates Random heads and tails"""
    tossed = random.randint(0,1)
    if tossed == 1:
        return heads
    else:
        return tails

heads_tosses = 0
tails_tosses = 0

while True:
    toss_again = raw_input("Do you want to play 'Guess Heads or Tails'[y, N]? ")
    try:
        if toss_again[0].lower() == 'y':
            tossed = toss()
            if heads:
                heads_tosses += 1
            else:
                tails_tosses += 1
                
            guessed = guess()
            
            compare(tossed = tossed, guessed = guessed)
        elif toss_again[0].lower() == 'n':
            break
    except:
        print "Oops, try again"
        
print("that was fun, the heads/tails percentage was %s/%s" % ((heads_tosses/(heads_tosses + tails_tosses) * 100),(tails_tosses/(heads_tosses + tails_tosses) * 100)))
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    