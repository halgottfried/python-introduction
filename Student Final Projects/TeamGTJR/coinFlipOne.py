#!/usr/bin/python

__author__ = "Tracy Hockenhull"

'''
This file will contain a function called flipOne() that will return either
an "H" or a "T" to its caller.
'''


import random

def flipOne():
    flipped = random.randint(0,1)
    # print flipped        # debug flipOne()
    if flipped == 0:
        flipped = "H"
        # This converts the numeric output to a single-letter string representing heads.
    else:
        flipped = "T"
        # This converts the numeric output to a single-letter string representing tails.
    # print flipped           #debug if loop
    return flipped

flipOne()
    

