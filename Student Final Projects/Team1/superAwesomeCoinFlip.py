#superAwesomeCoinFlip
# This module is responsible for running the coin flip program
__author__ = 'akeemakinola'

# import modules
import moduleDisplay

# Run the program
print "... START ..."
moduleDisplay.displayOutcome()
print ".... END ...."
