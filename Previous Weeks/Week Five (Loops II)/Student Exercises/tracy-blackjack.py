#Blackjack----DEALER STANDS ALL THE TIME
#random integer between 1 and 11 (dealer)
#another random integer, also between 1 and 11 (dealer)
#generate 3rd number, integer, between 1 and 11 (player)
#generate 4th number, integer, between 1 and 11 (player)
#show one dealer number
#show both player numbers
#ask for user input for hit/stand
#for stand, compare A numbers to B number
#for hit, generate another random integer between 1 and 11 and compare to A numbers

import random

#generate dealer cards
dealer_1 = random.randint(1,11)
dealer_2 = random.randint(1,11)

dealer_hand = dealer_1 + dealer_2   #calculate dealer hand value
#dealer_hand = 22   #debug

# Show the dealer's shown card (#2)
print ("The dealer's showing a card with value " + str(dealer_2) + ".")
#print dealer_1
print ("Dealer's hand value is " + str(dealer_hand) + ".")  #debug-show dealer's hand value

player_1 = random.randint(1,11)
player_2 = random.randint(1,11)

#calculate player's hand value
player_hand = player_1 + player_2
#player_hand = 22    #debug greater than 21 condition

#display player card values and hand value.
print ("Your cards have values of " + str(player_1) + " and " + str(player_2) + ", and the total value is " + str(player_hand) + ".")

game_status = 0   #NEED THIS SO THAT WE CAN EXIT THE WHILE LOOP? game_status = 0 means still in play. game_status = 1 means not in play

while game_status != 1:   #While the game is still in play
    
    while player_hand<=21 and dealer_hand<=21:           #CHECKING TO MAKE SURE PLAYER IS NOT BUSTED
    
        hit_or_stand = raw_input("Do you want to hit (hit) or stand (stand)? ")
    
        if hit_or_stand == "stand":
            
            if dealer_hand > player_hand:   #set so that the player wins even if there's a tie.
                print ("Dealer wins!")
                game_status = 1
                break
            
            else:
                print ("You win!")
                game_status = 1
                break
        
        elif hit_or_stand =="hit":
            player_new = random.randint(1,11)
            player_hand = player_hand + player_new
            print player_hand
            
        else:
            print ("I didn't understand that.")
            
        
    #This tells whether the player or the dealer is busted.
    if player_hand>21:
        print ("You are busted!")
       # break
    elif dealer_hand>21:
        print ("The dealer is busted!")
        #break
    else:
        break
        #print ("Tracy doesn't know what she's doing.")
        #don't need to do anything to exit the while loop
        