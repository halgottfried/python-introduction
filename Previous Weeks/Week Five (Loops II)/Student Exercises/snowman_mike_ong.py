# Make a word guessing game using functions
snow_1 = "      _     "
snow_2 = "    _|=|_   "
snow_3 = "    ('')    "
snow_4 = ">--( o  )--<"
snow_5 = "  ( o    )  "


snow_man = [snow_1, snow_2, snow_3, snow_4, snow_5]

# answer can NOT use the character "_" (underscore)
# answer can NOT include repeated letters (i.e. "class" -- 2 "s" -- or "kauffman" -- 2 "f")
answers_list = [ "python", "hacker", "frosty" ]

# randomly select from answers_list
# answer = "python"
import random
answer = answers_list[ random.randint( 0, len( answers_list ) - 1 ) ]

wrong_guesses = []
correct_guesses = []
for index in range( len( answer ) ):
	#print index
	correct_guesses.append( "_" )

def print_snow(snow_man):
	for snow in snow_man:
		print snow

def print_answer(answer):
	output = ""
	for index in range( len( correct_guesses ) ):
		output = output + correct_guesses[ index ] + " "
	print output
		
	# print prior guesses
	output = ""
	for index in range( len( wrong_guesses ) ):
		output = output + wrong_guesses[ index ] + " "
	print( "Your wrong guesses: " + output )

print_snow( snow_man )

while ( len( snow_man ) > 0 ) & ( "_" in correct_guesses ) :
	print("I am thinking of a word:")
	print_answer(answer)
	
	print( "Incorrect guesses will melt your snowman." )

	guess = raw_input("Guess a letter: ")

	if guess in answer:
		print( "\nGood guess!" )
		
		# TODO - make it possible to use answer words that repeat letters
		correct_guesses[ answer.find( guess ) ] = guess
	else:
		#snow_man.remove(snow_man[0])
		# or you can use--
		if len( snow_man ) > 0 :
			snow_man.pop()
		print_snow(snow_man)
		wrong_guesses.append( guess )
		

if len( snow_man ) == 0 :
	print "Sorry, you lose. Try again!"
else:
	print "You won!"